---

nginx_error_log: "/var/log/nginx/error.log"
nginx_access_log: "/var/log/nginx/access.log main buffer=16k"
nginx_server_tokens: "off"
nginx_multi_accept: "on"

nginx_extra_http_options: |
  proxy_buffering    off;
  proxy_set_header   X-Real-IP       $remote_addr;
  proxy_set_header   X-Scheme        $scheme;
  proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header   Host            $http_host;

  proxy_cache_path
    /var/cache/nginx/studio
    levels=1:2
    keys_zone=studio_cache:10m
    max_size=50m
    inactive=60m
    use_temp_path=off;

  proxy_cache_path
    /var/cache/nginx/preview
    levels=1:2
    keys_zone=preview_cache:10m
    max_size=50m
    inactive=60m
    use_temp_path=off;

  # # rewrite preview url
  # map $request_uri $preview {
  #   default                                               0;
  #   ~*^/preview\?(.*)view=fragmentPreview(.*)$           /cae-preview/servlet$request_uri;
  # }
  # # exchange /blueprint with /cae-preview
  # map $request_uri $blueprint {
  #   default                                               0;
  #   ~*^/blueprint(.*)$           /cae-preview$1;
  # }

nginx_upstreams:
  - name: solr_be
    servers:
      - 127.0.0.1:40080 max_fails=3 fail_timeout=30s

  - name: cms_ior
    servers:
      - 127.0.0.1:40180 max_fails=3 fail_timeout=30s

  - name: wfs_ior
    servers:
      - 127.0.0.1:40380 max_fails=3 fail_timeout=30s

  - name: studio_server
    servers:
      - 127.0.0.1:41080 max_fails=3 fail_timeout=30s

  - name: studio_client
    servers:
      - 127.0.0.1:8888 max_fails=3 fail_timeout=30s

  - name: cae_preview
    servers:
      - 127.0.0.1:40980 max_fails=3 fail_timeout=30s

  - name: content_feeder
    servers:
      - 127.0.0.1:40480 max_fails=3 fail_timeout=30s

  - name: cms_actuator
    servers:
      - 127.0.0.1:40181 max_fails=3 fail_timeout=30s

  - name: wfs_actuator
    servers:
      - 127.0.0.1:40381 max_fails=3 fail_timeout=30s

  - name: contentfeeder_actuator
    servers:
      - 127.0.0.1:40181 max_fails=3 fail_timeout=30s

  - name: caefeeder_actuator
    servers:
      - 127.0.0.1:40781 max_fails=3 fail_timeout=30s

  - name: studio_actuator
    servers:
      - 127.0.0.1:41081 max_fails=3 fail_timeout=30s

  - name: elasticworker_actuator
    servers:
      - 127.0.0.1:40681 max_fails=3 fail_timeout=30s

  - name: userchanges_actuator
    servers:
      - 127.0.0.1:40581 max_fails=3 fail_timeout=30s

  - name: caepreview_actuator
    servers:
      - 127.0.0.1:40981 max_fails=3 fail_timeout=30s


nginx_vhosts:
  # redirects to https
  #
  - listen: 80
    server_name: "{{ ansible_fqdn }}"
    filename: 00-{{ ansible_fqdn }}.conf
    extra_parameters: |
      location = /nginx_status {
        stub_status on;
        access_log off;
        allow 127.0.0.1;
        deny all;
      }

      location / {
        return 301 https://preview.{{ ansible_domain }}$request_uri;
      }

  - listen: 80
    server_name: preview.{{ ansible_domain }} preview.{{ ansible_fqdn }}
    filename: 10-preview.{{ ansible_domain }}.conf
    extra_parameters: |
      location = /nginx_status {
        stub_status on;
        access_log off;
        allow 127.0.0.1;
        deny all;
      }

      location / {
        return 301 https://preview.{{ ansible_domain }}$request_uri;
      }

  - listen: 80
    server_name: studio.{{ ansible_domain }} overview.{{ ansible_fqdn }}
    filename: 10-studio.{{ ansible_domain }}.conf
    return: "301 https://studio.{{ ansible_domain }}$request_uri"


  # regulary vhost definitions
  #
  - listen: 443 ssl http2
    server_name: preview.{{ ansible_domain }}
    filename: '20-preview.{{ ansible_domain }}.conf'
    root: /var/www/html
    index: index.html
    access_log: '/var/log/nginx/preview_access.log'
    error_log:  '/var/log/nginx/preview_error.log'
    extra_parameters: |
      ssl_certificate     /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_certificate_key /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_protocols       TLSv1.1 TLSv1.2;
      ssl_ciphers         HIGH:!aNULL:!MD5;

      # if ($preview) {
      #   return 301 $preview;
      # }
      # if ($blueprint) {
      #   return 301 $blueprint;
      # }

      location ~ ^/internal/(.*) {
        return 403;
      }
      location ~ ^/blueprint/+servlet/+internal/(.*) {
        return 403;
      }
      location = / {
        rewrite ^/$ /blueprint/servlet/corporate redirect;
      }
      location ~ ^/blueprint/(.*) {
        try_files $uri @backend;
      }
      location @backend {
        add_header X-Backend "cae_preview";

        proxy_cache preview_cache;
        proxy_cache_revalidate on;
        proxy_cache_min_uses 3;
        proxy_cache_use_stale
            error timeout updating
            http_500 http_502 http_503 http_504;
        proxy_cache_background_update on;
        proxy_cache_lock on;

        proxy_pass         http://cae_preview;
        proxy_set_header   Host              $host;
        proxy_cookie_path  /blueprint /;
      }

  - listen: 443 ssl http2
    server_name: 'studio.{{ ansible_domain }}'
    filename: '20-studio.{{ ansible_domain }}.conf'
    root: /var/www/html
    index: index.html
    access_log: '/var/log/nginx/studio_access.log'
    error_log:  '/var/log/nginx/studio_error.log'
    extra_parameters: |
      ssl_certificate     /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_certificate_key /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_protocols       TLSv1.1 TLSv1.2;
      ssl_ciphers         HIGH:!aNULL:!MD5;

      # location = / {
      #   rewrite ^/$ /studio/$1 redirect;
      # }

      location = /rest/api/supported-locales.js {
        return 301 /api/supported-locales.js;
      }

      # /login AND /logout AND /api AND /rest
      # schould go to studio_server
      location ~* ^/(login|logout|api|rest) {
        add_header X-Backend "studio_server";

        proxy_pass         http://studio_server;
        proxy_set_header   Host              $host;
        proxy_cookie_path  /studio /;
      }

      location / {
        add_header X-Backend "studio_client";
        add_header X-Cache-Status $upstream_cache_status;

        #add_header X-Frame-Options SAMEORIGIN;
        #add_header X-Content-Type-Options nosniff;
        #add_header X-XSS-Protection "1; mode=block";
        #add_header Content-Security-Policy "default-src 'self' preview.cm.local; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; img-src 'self' *; style-src 'self' 'unsafe-inline'; font-src 'self' ; object-src 'none'; frame-src 'self' preview.cm.local; ";

        # This block will catch static file requests, such as images
        # The ?: prefix is a 'non-capturing' mark, meaning we do not require
        # the pattern to be captured into $1 which should help improve performance
        location ~* \.(?:jpg|jpeg|gif|png|ico|xml)$ {
          access_log        off;
          log_not_found     off;

          # The Expires HTTP header is a basic means of controlling caches; it tells all caches how long
          # the associated representation is fresh for. After that time, caches will always check back with
          # the origin server to see if a document is changed.
          #
          # "If a request includes the no-cache directive, it SHOULD NOT include min-fresh, max-stale, or max-age."
          # (source: http://www.ietf.org/rfc/rfc2616.txt, p114)
          #
          # Nginx automatically sets the `Cache-Control: max-age=t` header, if `expires` is present, where t is a time
          # specified in the directive, in seconds. Shortcuts for time can be used, for example 5m for 5 minutes.
          #
          expires           5m;

          # public:           marks authenticated responses as cacheable; normally, if HTTP authentication is required,
          #                   responses are automatically private.
          #
          add_header        Cache-Control "public";

          proxy_pass         http://studio_client;
        }

        # This block will catch static file requests of fonts and allows fonts to be requested via CORS
        # The ?: prefix is a 'non-capturing' mark, meaning we do not require
        # the pattern to be captured into $1 which should help improve performance
        location ~* \.(?:eot|woff|woff2|ttf|svg|otf) {
          access_log        off;
          log_not_found     off;

          # The Expires HTTP header is a basic means of controlling caches; it tells all caches how long
          # the associated representation is fresh for. After that time, caches will always check back with
          # the origin server to see if a document is changed.
          #
          # "If a request includes the no-cache directive, it SHOULD NOT include min-fresh, max-stale, or max-age."
          # (source: http://www.ietf.org/rfc/rfc2616.txt, p114)
          #
          # Nginx automatically sets the `Cache-Control: max-age=t` header, if `expires` is present, where t is a time
          # specified in the directive, in seconds. Shortcuts for time can be used, for example 5m for 5 minutes.
          #
          expires           5m;

          # public:           marks authenticated responses as cacheable; normally, if HTTP authentication is required,
          #                   responses are automatically private.
          #
          add_header        Cache-Control "public";

          # allow CORS requests
          add_header        Access-Control-Allow-Origin *;

          types     {font/opentype otf;}
          types     {application/vnd.ms-fontobject eot;}
          types     {font/truetype ttf;}
          types     {application/font-woff woff;}
          types     {font/x-woff woff2;}

          proxy_pass         http://studio_client;
        }

        proxy_cache studio_cache;
        proxy_cache_revalidate on;
        proxy_cache_min_uses 3;
        proxy_cache_use_stale
            error timeout updating
            http_500 http_502 http_503 http_504;
        proxy_cache_background_update on;
        proxy_cache_lock on;

        proxy_pass         http://studio_client;
        proxy_set_header   Host              $host;
        proxy_cookie_path  /studio /;
      }



  # proxy to internal services
  #
  - listen: 80
    server_name: solr-be.{{ ansible_domain }}
    filename: 80-solr-be.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/solr-be_access.log'
    error_log:  '/var/log/nginx/solr-be_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "solr-be";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://solr_be/solr/;
      }

  - listen: 80
    server_name: content-feeder.{{ ansible_domain }}
    filename: 80-content-feeder.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/content-feeder_access.log'
    error_log:  '/var/log/nginx/content-feeder_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "content-feeder";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://content_feeder/admin;
      }

  - listen: 80
    server_name: cms.ior.{{ ansible_domain }}
    filename: 80-cms.ior.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/cms-ior_access.log'
    error_log:  '/var/log/nginx/cms-ior_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "cms-ior";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://cms_ior/ior;
      }

  - listen: 80
    server_name: wfs.ior.{{ ansible_domain }}
    filename: 80-wfs.ior.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/wfs-ior_access.log'
    error_log:  '/var/log/nginx/wfs-ior_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "wfs-ior";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://wfs_ior/ior;
      }


  - listen: 80
    server_name: cms.actuator.{{ ansible_domain }}
    filename: 80-cms.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/cms-actuator_access.log'
    error_log:  '/var/log/nginx/cms-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "cms-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://cms_actuator/actuator/;
      }

  - listen: 80
    server_name: wfs.actuator.{{ ansible_domain }}
    filename: 80-wfs.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/wfs-actuator_access.log'
    error_log:  '/var/log/nginx/wfs-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "wfs-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://wfs_actuator/actuator/;
      }

  - listen: 80
    server_name: content-feeder.actuator.{{ ansible_domain }}
    filename: 80-content-feeder.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/content-feeder-actuator_access.log'
    error_log:  '/var/log/nginx/content-feeder-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "content-feeder-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://contentfeeder_actuator/actuator/;
      }

  - listen: 80
    server_name: caefeeder-preview.actuator.{{ ansible_domain }}
    filename: 80-caefeeder-preview.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/caefeeder-preview-actuator_access.log'
    error_log:  '/var/log/nginx/caefeeder-preview-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "caefeeder-preview-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://caefeeder_actuator/actuator/;
      }

  - listen: 80
    server_name: studio-server.actuator.{{ ansible_domain }}
    filename: 80-studio.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/studio-actuator_access.log'
    error_log:  '/var/log/nginx/studio-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "studio-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://studio_actuator/actuator/;
      }

  - listen: 80
    server_name: elastic-worker.actuator.{{ ansible_domain }}
    filename: 80-elastic-worker.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/elastic-worker-actuator_access.log'
    error_log:  '/var/log/nginx/elastic-worker-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "elastic-worker-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://elasticworker_actuator/actuator/;
      }

  - listen: 80
    server_name: user-changes.actuator.{{ ansible_domain }}
    filename: 80-user-changes.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/user-changes-actuator_access.log'
    error_log:  '/var/log/nginx/user-changes-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "user-changes-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://userchanges_actuator/actuator/;
      }

  - listen: 80
    server_name: cae-preview.actuator.{{ ansible_domain }}
    filename: 80-cae-preview.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/cae-preview-actuator_access.log'
    error_log:  '/var/log/nginx/cae-preview-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "cae-preview-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://caepreview_actuator/actuator/;
      }

