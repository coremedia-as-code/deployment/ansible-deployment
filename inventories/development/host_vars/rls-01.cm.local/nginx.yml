---

nginx_error_log: "/var/log/nginx/error.log warn"
nginx_access_log: "/var/log/nginx/access.log main buffer=16k"
nginx_server_tokens: "off"
nginx_multi_accept: "on"

nginx_extra_http_options: |
  proxy_buffering    off;
  proxy_set_header   X-Real-IP $remote_addr;
  proxy_set_header   X-Scheme $scheme;
  proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header   Host $http_host;

nginx_upstreams:

  - name: overview
    servers:
      - 127.0.0.1:8888 max_fails=3 fail_timeout=30s

  - name: solr_slave
    servers:
      - 127.0.0.1:40080 max_fails=3 fail_timeout=30s

  - name: cae_live
    servers:
      - 127.0.0.1:42180 max_fails=3 fail_timeout=30s
      - 127.0.0.1:42280 max_fails=3 fail_timeout=30s

  - name: solr_master
    servers:
      - solr-master.cm.local:40080 max_fails=3 fail_timeout=30s

  # IORs
  - name: rls_ior
    servers:
      - 127.0.0.1:42080 max_fails=3 fail_timeout=30s

  - name: mls_ior
    servers:
      - mls.cm.local:40280 max_fails=3 fail_timeout=30s

  # sprint-boot actuators
  - name: mls_actuator
    servers:
      - mls.cm.local:40281 max_fails=3 fail_timeout=30s

  - name: caefeeder_actuator
    servers:
      - mls.cm.local:40881 max_fails=3 fail_timeout=30s

  - name: rls_actuator
    servers:
      - 127.0.0.1:42081 max_fails=3 fail_timeout=30s

  - name: caelive_1_actuator
    servers:
      - 127.0.0.1:42181 max_fails=3 fail_timeout=30s

  - name: caelive_2_actuator
    servers:
      - 127.0.0.1:42281 max_fails=3 fail_timeout=30s

  - name: caelive_3_actuator
    servers:
      - 127.0.0.1:42381 max_fails=3 fail_timeout=30s


nginx_vhosts:

  # redirects to https
  #
  - listen: 80
    server_name: _ "{{ ansible_fqdn }}"
    filename: 00-{{ ansible_fqdn }}.conf
    extra_parameters: |
      location = /nginx_status {
        stub_status on;
        access_log off;
        allow 127.0.0.1;
        deny all;
      }

      location / {
        return 301 https://corporate.{{ ansible_domain }}$request_uri;
      }

  - listen: 80
    server_name: overview.{{ ansible_domain }}
    filename: 10-overview.{{ ansible_domain }}.conf
    root: /var/www/html/overview
    index: index.html
    access_log: '/var/log/nginx/overview_access.log'
    error_log:  '/var/log/nginx/overview_error.log'
    extra_parameters: |

      location / {
        add_header         X-Backend         "overview";
        proxy_set_header   Host              $host;
        proxy_pass         http://overview/;
      }

      #location / {
      #  return 301 https://overview.{{ ansible_domain }}$request_uri;
      #}

  - listen: 80
    server_name: corporate.{{ ansible_domain }}
    filename: 10-corporate.{{ ansible_domain }}.conf
    return: "301 https://corporate.{{ ansible_domain }}$request_uri"

  # regulary vhost definitions
  #
  - listen: 443 ssl http2
    server_name: corporate.{{ ansible_domain }}
    filename: 20-corporate.{{ ansible_domain }}.conf
    root: /var/www/html
    index: index.html
    access_log: '/var/log/nginx/corporate_access.log'
    error_log:  '/var/log/nginx/corporate_error.log'
    rewrite_log: true
    extra_parameters: |
      ssl_certificate     /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_certificate_key /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_protocols       TLSv1.1 TLSv1.2;
      ssl_ciphers         HIGH:!aNULL:!MD5;

      location ~ ^/blueprint/+servlet/+internal/(.*) {
        return 403;
      }
      location ~ ^/internal/(.*) {
        return 403;
      }
      location ~ ^/robots(.*) {
        rewrite ^/robots.txt$ /service/robots/corporate last;
      }

      # only CAE_lice_1 knows about sitemap
      #
      location = /sitemap_index.xml {
        add_header         X-Backend         "cae_live";
        add_header         X-Upstream        $upstream_addr;

        proxy_set_header   Host              $host;
        proxy_set_header   X-Real-IP         $remote_addr;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;

        proxy_pass         http://127.0.0.1:42180/blueprint/servlet/service/sitemap/abffe57734feeee/sitemap_index.xml;
      }
      location = /.*sitemap(.*).xml.gz$ {
        add_header         X-Backend         "cae_live";
        add_header         X-Upstream        $upstream_addr;

        proxy_set_header   Host              $host;
        proxy_set_header   X-Real-IP         $remote_addr;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;

        proxy_pass         http://127.0.0.1:42180/blueprint/servlet/service/sitemap/abffe57734feeee/sitemap$1.xml.gz;
      }

      location = / {
        rewrite ^/$ /corporate redirect;
      }
      location ~ ^/(.*) {
        add_header         X-Backend         "cae_live";
        add_header         X-Upstream        $upstream_addr;

        proxy_set_header   Host              $host;
        proxy_set_header   X-Real-IP         $remote_addr;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;

        proxy_cookie_path  /blueprint/ /;

        proxy_pass         http://cae_live/blueprint/servlet/$1;
      }

  # CoreMedia overview site
  #
  - listen: 443 ssl http2
    state: absent
    server_name: overview.{{ ansible_domain }}
    filename: 50-overview.{{ ansible_domain }}.conf
    root: /var/www/html/overview
    index: index.html
    access_log: '/var/log/nginx/overview_access.log'
    error_log:  '/var/log/nginx/overview_error.log'
    extra_parameters: |
      ssl_certificate     /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_certificate_key /etc/coremedia/snakeoil/cm.local/cm.local.pem;
      ssl_protocols       TLSv1.1 TLSv1.2;
      ssl_ciphers         HIGH:!aNULL:!MD5;

      location / {
        add_header         X-Backend         "overview";
        #add_header         X-Upstream        $upstream_addr;

        proxy_set_header   Host              $host;
        #proxy_set_header   X-Real-IP         $remote_addr;
        #proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;

        # proxy_cookie_path  /blueprint/ /;

        proxy_pass         http://overview;
      }


      # location / {
      #   if (-f $document_root/system/maintenance.html) {
      #     return 503;
      #   }
      #   try_files $uri $uri/index.html $uri.html =404;
      # }

  # proxy to internal services
  #
  - listen: 80
    server_name: solr-master.{{ ansible_domain }}
    filename: 80-solr-master.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/solr-master_access.log'
    error_log:  '/var/log/nginx/solr-master_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "solr-master";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://solr_master/solr/;
      }

  - listen: 80
    server_name: solr-slave.{{ ansible_domain }}
    filename: 80-solr-slave.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/solr-slave_access.log'
    error_log:  '/var/log/nginx/solr-slave_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "solr-slave";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://solr_slave/solr/;
      }

  - listen: 80
    server_name: rls.ior.{{ ansible_domain }}
    filename: 80-rls.ior.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/rls-ior_access.log'
    error_log:  '/var/log/nginx/rls-ior_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "rls-ior";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://rls_ior/ior;
      }

  - listen: 80
    server_name: mls.ior.{{ ansible_domain }}
    filename: 80-mls.ior.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/mls-ior_access.log'
    error_log:  '/var/log/nginx/mls-ior_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "mls-ior";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://mls_ior/ior;
      }

  - listen: 80
    server_name: rls.actuator.{{ ansible_domain }}
    filename: 80-rls.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/rls-actuator_access.log'
    error_log:  '/var/log/nginx/rls-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "rls-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://rls_actuator/actuator/;
      }

  - listen: 80
    server_name: mls.actuator.{{ ansible_domain }}
    filename: 80-mls.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/mls-actuator_access.log'
    error_log:  '/var/log/nginx/mls-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "mls-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://mls_actuator/actuator/;
      }

  - listen: 80
    server_name: caefeeder-live.actuator.{{ ansible_domain }}
    filename: 80-caefeeder-live.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/caefeeder-live-actuator_access.log'
    error_log:  '/var/log/nginx/caefeeder-live-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "caefeeder-live-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://caefeeder_actuator/actuator/;
      }

  - listen: 80
    server_name: cae1.actuator.{{ ansible_domain }}
    filename: 80-cae1.actuator.{{ ansible_domain }}.conf
    root: /var/www/
    index: index.html
    access_log: '/var/log/nginx/cae1-actuator_access.log'
    error_log:  '/var/log/nginx/cae1-actuator_error.log'
    extra_parameters: |
      location / {
        add_header X-Backend "cae-live-actuator";
        add_header Access-Control-Allow-Origin *;
        proxy_pass http://caelive_1_actuator/actuator/;
      }



  # ---

#      location / {
#        add_header X-Backend "grafana";
#        proxy_pass http://grafana/;
#        proxy_http_version 1.1;
#        proxy_set_header Upgrade $http_upgrade;
#        proxy_set_header Connection "upgrade";
#        proxy_set_header X-Real-IP $remote_addr;
#        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#        proxy_set_header Host $host;
#        proxy_redirect off;
#      }
#
#
#      location /dist {
#        allow 127.0.0.1;
#        allow 172.20.6.165;
#        allow 172.20.6.166;
#        allow 172.20.6.169;
#        allow 172.20.6.170;
#        allow 172.20.6.171;
#        deny all;
#      }

# -------------------------------------------------------------------------------------------------
