#!/usr/bin/env bash

PIP=$(command -v pip)

if [ -f "python-requirements.txt" ]
then
  echo "install python requirements"
  ${PIP} install --requirement python-requirements.txt > /dev/null
fi
