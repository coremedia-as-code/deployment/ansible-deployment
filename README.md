# ansible deployment for CoreMedia components

ansible environment to deploy an full coremedia stack.

this deployment can use *war* or *zip* artefacts, expectly from the *deployment-archive.zip* or *docker* containers.

## Deployment Steps

1. install requirements
2. prepare destination systems
   1. prepare databases
   2. install licenses
3. build artefacts
4. install CoreMedia on destination systems
   - as docker
   - as springboot
5. install monitoring

## prepare your local system

i use simple *hosts* entries for the deployment.

```
$ sudo cat ./hosts >> /etc/hosts
```

## vault support

All Passwords or Credentials are stored in an *ansible vault*

To create an strong, random password-file, you can use `openssl`
```
$ mkdir vault
$ openssl rand -base64 2048 > vault/ansible-vault.pass
```

Then you can create and edit the vault
```
$ ansible-vault create vault --vault-password-file vault/ansible-vault.pass
$ ansible-vault edit   vault --vault-password-file vault/ansible-vault.pass
```

A short Test
```
$ ansible -i hosts -m debug -a 'var=hostvars[inventory_hostname]' monitoring --vault-password-file vault/ansible-vault.pass
```

## Steps ...

### install requirements

All needed ansible roles are stored in external repositories.

`ansible-galaxy` will download all roles in the project `roles` directory.

```
ansible-galaxy install -r requirements.yml [--force]
```

### prepare destination hosts

For installation of licenses, databases and java.

implements also iptables rules for better security.

```
ansible-playbook --inventory inventories/developemnt --vault-password-file vault/ansible-vault.pass playbooks/prepare.yml
```

### build artefacts

**For an successful build, you need an CoreMedia Account**

You can export the followinf environment variables,

- `RELEASE_URL_USERNAME`
- `RELEASE_URL_PASSWORD`

or create an environment file:

```
$ cat coremedia_build.yaml
---
coremedia_build_release: cmcc-10
coremedia_build_workspace_version: 2007.2

release_url_username: "coremedia user"
release_url_password: "coremedia password"
```

After them, you can start the build process

```
$ ansible-playbook --inventory inventories/development -e @coremedia_build.yaml --vault-password-file vault/ansible-vault.pass playbooks/build.yml
```


### install CoreMedia on destination systems

deploy all CoreMedia applications and start them.

```
time ansible-playbook --inventory hosts --extra-vars @coremedia-config.yml --vault-password-file vault/ansible-vault.pass deployment.yml

...

real    2m58,096s
user    2m18,370s
sys     0m19,735s
```






## spring-boot deployment

### content repository

A content repository like nexus or artifactory are not needed. You cant simple use an nginx or apache as webserver.

All needed files should be in one directory like this example for version *1904.2*:

```
files/1904.2
├── files/1904.2/caefeeder-live.war
├── files/1904.2/caefeeder-preview.war
├── files/1904.2/caefeeder-tools-application.zip
├── files/1904.2/cae-live.war
├── files/1904.2/cae-preview.war
├── files/1904.2/catalina-jmx-remote.jar
├── files/1904.2/cms-tools-application.zip
├── files/1904.2/content-feeder.war
├── files/1904.2/content-management-server.war
├── files/1904.2/content-users.zip
├── files/1904.2/coremedia-tomcat.jar
├── files/1904.2/cs-license.zip
├── files/1904.2/editor-webstart.war
├── files/1904.2/elastic-worker.war
├── files/1904.2/index.html
├── files/1904.2/master-live-server.war
├── files/1904.2/mls-license.zip
├── files/1904.2/mls-tools-application.zip
├── files/1904.2/replication-live-server.war
├── files/1904.2/rls-license.zip
├── files/1904.2/rls-tools-application.zip
├── files/1904.2/solr-config.zip
├── files/1904.2/studio.war
├── files/1904.2/theme-importer-application.zip
├── files/1904.2/user-changes.war
├── files/1904.2/wfs-tools-application.zip
└── files/1904.2/workflow-server.war
```

You can use the [integrated example docker container](https://gitlab.com/coremedia-as-code/deployment/content-repository) or implement an own service.


## prepare your local system

i use simple *hosts* entries for the deployment.

```
192.168.124.1           repository.cm.local
192.168.124.20          backend.cm.local
192.168.124.30          frontend.cm.local
192.168.124.35          delivery.cm.local
192.168.124.50          monitoring.cm.local

192.168.124.35          overview.cm.local
192.168.124.35          corporate.cm.local
192.168.124.35          solr-master.cm.local  solr-slave.cm.local  rls.ior.cm.local mls.ior.cm.local
192.168.124.20          studio.cm.local
192.168.124.20          solr-be.cm.local                content-feeder.cm.local
192.168.124.20          preview.cm.local
192.168.124.20          cms.cm.local
192.168.124.20          wfs.cm.local
192.168.124.30          mls.cm.local
192.168.124.30          rls.cm.local

192.168.124.110         dba-backend.cm.local
192.168.124.100         dba-frontend.cm.local
```

These allow me to access the locally rolled out environment directly.


## vault support

All Passwords or Credentials are stored in an *ansible vault*

```
$ mkdir vault
$ openssl rand -base64 2048 > ansible-vault.pass

$ ansible-vault create vault --vault-password-file vault/ansible-vault.pass
$ ansible-vault edit   vault --vault-password-file vault/ansible-vault.pass

ansible -i hosts -m debug -a 'var=hostvars[inventory_hostname]' monitoring --vault-password-file vault/ansible-vault.pass

```

## Deployment

### download all playbooks from different repositories

```
ansible-galaxy install -r requirements.yml [--force]
```

## prepare destination hosts

For installation of licenses, java and databases.

implements also iptables rules for better security.

```
ansible-playbook --inventory inventories/coremedia --vault-password-file vault/ansible-vault.pass playbooks/prepare.yml

time ansible-playbook --inventory hosts --extra-vars @coremedia-config.yml --vault-password-file vault/ansible-vault.pass prepare.yml

...

real    3m34,112s
user    1m42,589s
sys     0m13,487s
```

## deploy CoreMedia services

deploy all CoreMedia applications and start them.

```
time ansible-playbook --inventory hosts --extra-vars @coremedia-config.yml --vault-password-file vault/ansible-vault.pass deployment.yml

...

real    2m58,096s
user    2m18,370s
sys     0m19,735s
```

## Content Import

run the demo content importer.

```
time ansible-playbook --inventory hosts --extra-vars @coremedia-config.yml --vault-password-file vault/ansible-vault.pass content-import.yml

...

real    5m36,875s
user    1m28,528s
sys     0m4,202s
```

